package ecommerce.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;




@Repository
public interface ProductRepository extends JpaRepository<ProductDb,Long>,JpaSpecificationExecutor<ProductDb>{

	List<ProductDb> findByVisibilty(boolean visibilty);
	List<ProductDb> findByUserid(Long userid);
	List<ProductDb> findByCategory(String Category);
}
