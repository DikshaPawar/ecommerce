package ecommerce.product.Filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import ecommerce.product.ProductDb;
import ecommerce.product.Product_;


public class SearchCriteria implements Specification<ProductDb>{

	
	private ProductFilter filter;
	
	public SearchCriteria(ProductFilter filter)
	{
		this.filter=filter;
	}

	@Override
	public Predicate toPredicate(Root<ProductDb> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		
		List<Predicate> conditions=new ArrayList<>();

		if(filter.getMaxprice()!=null && filter.getMinprice()!=null)
			conditions.add(cb.between(root.get(Product_.PRICE),filter.getMinprice(),filter.getMaxprice()));
		else if(filter.getMaxprice()!=null)
			conditions.add(cb.lessThanOrEqualTo(root.get(Product_.PRICE),filter.getMaxprice()));
		else if(filter.getMinprice()!=null)
			conditions.add(cb.greaterThanOrEqualTo(root.get(Product_.PRICE), filter.getMinprice()));
		if(filter.getIsdeleted()!=null)
			conditions.add(cb.equal(root.get(Product_.DELETED),filter.getIsdeleted()));
		if(filter.getName()!=null)
			conditions.add(cb.like(root.get(Product_.NAME), "%"+filter.getName()+"%"));
		if(filter.getCategory()!=null)
			conditions.add(cb.like(root.get(Product_.CATEGORY),"%"+filter.getCategory().substring(0,1).toUpperCase()+"%"));
		
		return cb.and(conditions.toArray(new Predicate[0]));
	}
	
	
}
