package ecommerce.product.Filter;
import java.util.List;

import org.springframework.stereotype.Component;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class ProductFilter {
	
	Boolean isdeleted;
	Integer minprice;
	Integer maxprice;
	String category;
	String name;
}
