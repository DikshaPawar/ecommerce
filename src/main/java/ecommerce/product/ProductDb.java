package ecommerce.product;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.annotations.Type;
import org.springframework.stereotype.Controller;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="Product")
@Controller
@org.hibernate.annotations.TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class ProductDb {
	
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pid;
	 
	@Column		
	private String name;
	
	@Column		
	private String category;  
	
	
	@Column		
	private Long userid;
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")	
	private AddInfo info;
	
	@Column		
	private Float actualprice;    
	
	@Column		
	private Float listedprice;
	
	@Column
	private Boolean visibilty;
	
	@Column
	private Boolean isdeleted;
	
	public boolean getVisibilty() {
		return this.visibilty;
	}
	
	public boolean getIsdeleted() {
		return this.isdeleted;
	}
	
	
}
