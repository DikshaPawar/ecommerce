package ecommerce.product;


import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.google.gson.annotations.SerializedName;

import ecommerce.review.ReviewDataModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class ProductDataModel {
	
	@Autowired
	@NotNull
	@Valid
	private AddInfo info;
	
	public enum Category {
		@SerializedName("E")
	    ELECTRONICS,
	    @SerializedName("F")
		FASHION,
		@SerializedName("G")
		GROCERY,
		@SerializedName("B")
		BOOKS,
		@SerializedName("S")
		SPORTS,
		@SerializedName("O")
		OTHERS
	}
	private Long pid;
	
	@NotNull
	@Min(1)
	private Integer actualprice;
	
	@NotNull
	@Min(1)
	private Integer listedprice;
	
	
	private Long userid;
	
	@NotNull
	@Size(min=2,max=30)
	private String name;
	
	@NotNull
	private Category category;
	
	@NotNull
	private Boolean visibilty;
	
	
	private Boolean isdeleted;
	private Float avgrating;
	private List<ReviewDataModel> reviewlist;
	
	public boolean getVisibilty() {
		return this.visibilty;
	}

	public boolean getIsdeleted() {
		return this.isdeleted;
	}
}
