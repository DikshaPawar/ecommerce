package ecommerce.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.product.ProductDataModel.Category;
import ecommerce.product.Filter.ProductFilter;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;



@RestController
@RequestMapping("/product")
@Validated
public class ProductController {

	@Autowired
	private ProductService service;
	
	
	@PostMapping("/save")
	@Roles(roles=Role.SELLER)
	public ProductDataModel putProduct(@RequestBody @Valid ProductDataModel product) throws CustomException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.addProduct(userid,product);
		
	}
	
	@PostMapping("/update")
	@Roles(roles=Role.SELLER)
	public ProductDataModel updateProduct(@RequestBody ProductDataModel product) throws Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.updateProduct(userid,product);
	}
	
	@PostMapping("/bulk-save")
	@Roles(roles=Role.SELLER)
	public List<ProductDataModel> putProducts(@RequestBody @Valid List<ProductDataModel> products) throws Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.addProducts(userid,products);
		
	}
	
	@PostMapping("/delete")
	@Roles(roles=Role.SELLER)
	public List<ProductDataModel> deleteProduct(@RequestBody ProductDataModel product) throws Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.deleteProduct(userid, product);
	}

	@PostMapping("/list")
	public List<ProductDataModel> listAll(HttpServletRequest req,@RequestBody ProductFilter filters) throws Exception{
		Long userid=GetUserIdFilter.userid.get();
		return service.getProducts(userid,filters);

	}
	
	@GetMapping("/view")
	public ProductDataModel viewSingleProduct(@RequestParam(value="product",required=true) @NotNull @Min(1) Long pid) throws Exception{
		return service.viewSingleProduct(pid);
	}

	@GetMapping("/viewcategory")
	public List<ProductDataModel> viewProductByCategory(@RequestParam(value="category",required=true) @NotNull @Size(min=2,max=15) String category) throws Exception{
		
		return service.viewProductByCategory(category);
	}
	
	@GetMapping("/getcategories")
	public Category[] getCategories() throws Exception{
		return service.getcategorires();
	}
	
	
}
