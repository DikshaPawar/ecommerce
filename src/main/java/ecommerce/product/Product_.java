package ecommerce.product;

import javax.annotation.Generated;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductDb.class)
public class Product_ {
	  public static final String PRICE="listedprice";
	  public static final String CATEGORY="category";
	  public static final String NAME = "name";
	  public static final String VISIBILITY="visibilty";
	  public static final String DELETED="isdeleted";
  
}
