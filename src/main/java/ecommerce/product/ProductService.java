package ecommerce.product;


import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import org.springframework.stereotype.Service;


import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.product.ProductDataModel.Category;
import ecommerce.product.Filter.ProductFilter;
import ecommerce.product.Filter.SearchCriteria;
import ecommerce.review.ReviewDataModel;
import ecommerce.review.ReviewService;
import ecommerce.user.UserDb;
import ecommerce.user.UserRepository;
import ecommerce.utils.EcommerceUtils;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repo;
	
	@Autowired
	private UserRepository userrepo;

	@Autowired
	private ReviewService reviewservice;
	
	@Caching(evict= {@CacheEvict(cacheNames = "productlist", key="#userid"),@CacheEvict(cacheNames = "singleproduct",allEntries=true),@CacheEvict(cacheNames = "productcategorylist", allEntries=true)})
	public ProductDataModel addProduct(Long userid,ProductDataModel product) throws Exception {
		
		String category=product.getCategory().toString();
		
	
		if(contains(category)==false)
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);   
		
		
		ProductDb pro= EcommerceUtils.genericConversion(setDefaultValues(product),new ProductDb());	
		pro.setUserid(userid);
	
		return convertToDataModel(repo.save(pro));  
		
	}
	
	public ProductDataModel setDefaultValues(ProductDataModel product) {
		AddInfo info=product.getInfo();
		info.setUnitssold(0);
		product.setAvgrating(0f);       
		product.setInfo(info);
		product.setReviewlist(null);  	  
		product.setIsdeleted(false);
		
		return product;
	}
	
	@Caching(evict= {@CacheEvict(cacheNames = "productlist", key="#userid"),@CacheEvict(cacheNames = "singleproduct", allEntries=true),@CacheEvict(cacheNames = "productcategorylist",allEntries=true )})
	public List<ProductDataModel> deleteProduct(Long userid,ProductDataModel product) throws Exception {
		
		List<ProductDb> list=repo.findByUserid(userid);
		
		Long pid=product.getPid();
		

		if (pid==null) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		if (list.size()==0) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		
		boolean flag = true;
		
		List<ProductDataModel> updatedlist=new ArrayList<>();
		for(ProductDb pd:list) {
			if(pd.getPid()==pid) {
				pd.setIsdeleted(true);
				repo.save(pd);
				flag=false;
			}
			else if(pd.getIsdeleted()==false){
				updatedlist.add(convertToDataModel(pd));
			}
		}
		if(flag) {
			//seller trying to delete other seller product or pid does not match
			throw new CustomException(ErrorCodes.UPDATE_DENIED);
		}


		
		return updatedlist;
	}
	
	@Cacheable(value="productlist")
	public List<ProductDataModel> getProducts(Long userid,ProductFilter filter) throws Exception{
		
		UserDb user=userrepo.findOne(userid);
		String role=user.getRole();
		
		List<ProductDb> list=new ArrayList<>();
		if(role.equals("S")) {
			list=repo.findByUserid(userid);
		}
		else if (role.equals("B")) {
			list=repo.findByVisibilty(true);
			
		}
		if(list.size()==0 || list==null)
			throw new CustomException(ErrorCodes.PRODUCT_LIST_EMPTY);
		
		list=updatedList(list);
		
		List<ProductDb> filtered_list=new ArrayList<>();
		
		if(filter!=null)
		{
			SearchCriteria criteria=new SearchCriteria(filter);
			
			filtered_list=repo.findAll(criteria);			
				
		}
		
		if(filtered_list.size()==0|| filtered_list==null)
		{
			return (new ArrayList<>());
		}
		
		List<ProductDataModel> response_list=new ArrayList<>();
		
		for(ProductDb product:filtered_list)
		{
			response_list.add(convertToDataModel(product));
		}
		
		return response_list;
	}
	
	@Cacheable(value="prodcutcategorylist")
	public List<ProductDataModel> viewProductByCategory(String category) throws CustomException {
		
		if(category==null) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		if(contains(category)) {
			List<ProductDb> retrievedlist = repo.findByCategory(category.substring(0,1));
			List<ProductDataModel> newlist=new ArrayList<>();
			if(retrievedlist==null)
				throw new CustomException(ErrorCodes.PRODUCT_LIST_EMPTY);
			
			retrievedlist=updatedList(retrievedlist);
			
			for(ProductDb pd:retrievedlist) {
				if(pd.getVisibilty()==true)
					newlist.add(convertToDataModel(pd));
			}
			return newlist;
		}
		throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
	}
	
	public Category[] getcategorires(){
		return Category.values();
	}
	
	@Caching(evict= {@CacheEvict(cacheNames = "productlist", key="#userid"),@CacheEvict(cacheNames = "singleproduct", allEntries=true),@CacheEvict(cacheNames = "productcategorylist", allEntries=true)})
	public List<ProductDataModel> addProducts(Long userid,List<ProductDataModel> products) throws Exception {
			
			List<ProductDataModel> updatedproductlist=new ArrayList<>();
			for(ProductDataModel product:products) {
				updatedproductlist.add(addProduct(userid,product));
			}
			
			return updatedproductlist;
		}
	
	@Cacheable(value="singleprodcut")
	public ProductDataModel viewSingleProduct(Long pid) throws CustomException {
		
		if(pid==null) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		ProductDb object=repo.findOne(pid);
		if(object==null || object.getIsdeleted()==true || object.getVisibilty()==false) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		return convertToDataModel(object);
	}
	
	@Caching(evict= {@CacheEvict(cacheNames = "productlist", key="#userid"),@CacheEvict(cacheNames = "singleprodcut", allEntries=true),@CacheEvict(cacheNames = "prodcutcategorylist", allEntries=true)})
	public ProductDataModel updateProduct(Long userid,ProductDataModel product) throws Exception {
		
		UserDb user=userrepo.findOne(userid);
		String role=user.getRole();
		
		if(role.equals("B")) {
			throw new CustomException(ErrorCodes.UNAUTHORIZED_ACCESS);
		}
		
		Long pid=product.getPid();
		String category=product.getCategory().toString();
		String name=product.getName();
		product.setUserid(userid);
		
		ProductDb object=repo.findOne(pid);
		
		if (pid==null || category==null || name==null || contains(category)==false) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		if(object==null || object.getIsdeleted()==true) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		
		AddInfo info=product.getInfo();
		info.setUnitssold(object.getInfo().getUnitssold());
		product.setInfo(info);
		product.setAvgrating(0f);         
		product.setReviewlist(null);
		product.setIsdeleted(object.getIsdeleted());
		
		ProductDb updatedproduct=null;
		String cat=category.substring(0,1);
		
		if(object.getPid().equals(pid) && object.getCategory().toString().equals(cat) && object.getName().equals(name) && object.getUserid().equals(userid)) {
			ProductDb updatedProduct=EcommerceUtils.genericConversion(product,new ProductDb());
		    updatedproduct=repo.save(updatedProduct);
		}
		else {
			throw new CustomException(ErrorCodes.ACCESS_DENIED);
		}
	   
	    return convertToDataModel(updatedproduct);
	}
	
	public ProductDataModel convertToDataModel(ProductDb product) throws CustomException {
		
		ProductDataModel returnproduct= EcommerceUtils.genericConversion(product,new ProductDataModel());	
		
		Long pid =product.getPid();
	
		
		List<Integer> ratings=reviewservice.averageRatingOfProduct(pid);
		Integer totalreview=ratings.get(0);
		Integer totalscore=ratings.get(1);
		float rating=0;
		
		if(totalreview!=0)
			rating=totalscore/totalreview;
		
		List<ReviewDataModel> reviewlist=reviewservice.viewReviewOnProduct(pid);
		
		returnproduct.setAvgrating(rating);
		returnproduct.setReviewlist(reviewlist);
		
		return returnproduct;
	}
	

	
	public boolean contains(String s) {
		s=s.toUpperCase();
		for(Category choice:Category.values())
	           if (choice.name().equals(s)) 
	              return true;
	    return false;
	}

	public String getvalue(String category) {
		String cat="";
		for(Category choice:Category.values())
	        if (choice.name().substring(0,1).equals(category)) 
	        	 cat=choice.name();
		return cat;
	}
	
	public List<ProductDb> updatedList(List<ProductDb> prevlist){
		List<ProductDb> newlist=new ArrayList<>();
		for(ProductDb pd:prevlist) {
			if(pd.getIsdeleted()==false)
				newlist.add(pd);
		}
		return newlist;
	}

	
}
