package ecommerce.product;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class AddInfo implements Serializable{
	
	@NotNull
	private String imageurl;
	@NotNull
	private String description;
	private Integer unitssold;
	@NotNull
	@Min(1)
	private Integer unitsleft;
	
}