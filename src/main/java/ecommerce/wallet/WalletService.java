package ecommerce.wallet;

import java.io.UnsupportedEncodingException;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import lombok.extern.slf4j.Slf4j;



@Slf4j
@Service
public class WalletService {

	
	@Autowired
	private UserWalletRepository repo;

	@CacheEvict(cacheNames = "wallet", key="#retrievedUserId")
	public UserWalletDb addAmount(Long retrievedUserId,UserWalletDb wallet) throws CustomException, UnsupportedEncodingException{
		
		if(retrievedUserId==null) {
			log.error("Token is damaged");
			throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
		}
		Float amount=wallet.getAmount();
		
		if(amount<0 || amount>Integer.MAX_VALUE) {
			log.error("Invalid amount found :"+ amount);
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		else
		{
			UserWalletDb userWallet=repo.findByUserid(retrievedUserId);
			if(userWallet==null)
			{	log.error("User wallet not found in database");
				throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
			}
			userWallet.setAmount(userWallet.getAmount()+amount);
			return repo.save(userWallet);
		}
		
		
	}
	
	@Cacheable(value="wallet")
	public UserWalletDb getAmount(Long retrievedUserId) throws CustomException {
		if(retrievedUserId==null) {
			log.error("Token is damaged");
			throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
		}
		UserWalletDb user=repo.findByUserid(retrievedUserId);
		
		if(user==null)
		{	log.error("User wallet not found in database");
		throw new CustomException(ErrorCodes.NULL_VALUE);
		}	
		return user;
	}
	
}
