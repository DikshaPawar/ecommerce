package ecommerce.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserWalletRepository extends JpaRepository<UserWalletDb,Long> {

	UserWalletDb findByUserid(Long userid);
}
