package ecommerce.wallet;



import java.io.UnsupportedEncodingException;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;





@RestController
@RequestMapping("/users")
public class WalletController {
	
	@Autowired
	private WalletService service;
	
	@PostMapping("/add-amount")
	@Roles(roles=Role.BUYER)
	public UserWalletDb createWallet(@RequestBody UserWalletDb wallet) throws UnsupportedEncodingException, CustomException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.addAmount(userid,wallet);
	}
	
	@GetMapping("/get-amount")
	@Roles(roles=Role.BUYER)
	public UserWalletDb getAmount() throws CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return service.getAmount(userid);
	}
	
}
