package ecommerce.helper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.user.UserDb;
import ecommerce.utils.GetUserIdFilter;
import lombok.extern.slf4j.Slf4j;




@Slf4j
@Component
public class CommonInterceptor implements HandlerInterceptor{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		HandlerMethod hm;
		try {
			   hm = (HandlerMethod) handler;
		} catch (ClassCastException e) {
			   throw e;
		}
		Method method = hm.getMethod();
		if(method.getDeclaringClass().isAnnotationPresent(RestController.class)) {
			if(hm.getMethodAnnotation(Roles.class)!=null) {
				String role=GetUserIdFilter.role.get();
				Roles r=method.getAnnotation(Roles.class);
				if(r.roles().toString().substring(0,1).equals(role)) {
					return true;
				}
				else
					{	log.error("Found: "+role+" Required: "+r.roles().toString());
					throw new CustomException(ErrorCodes.UNAUTHORIZED_ACCESS);}
			}
			return true;
			
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
	}

}
