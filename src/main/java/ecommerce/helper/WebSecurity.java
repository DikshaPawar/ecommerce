package ecommerce.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import ecommerce.user.UserRepository;
import ecommerce.utils.Constants;
import ecommerce.utils.GetUserIdFilter;







@EnableWebSecurity
@Configuration
public class WebSecurity extends WebSecurityConfigurerAdapter {
	
	@Autowired
	Constants constant;
	
	@Autowired
	private CustomAuthService authenticationservice;
	
	@Autowired
    UserRepository usersRepo;

    
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and().authorizeRequests()
                .antMatchers(HttpMethod.POST, "/").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(),usersRepo))
                .addFilterBefore(new GetUserIdFilter(),JWTAuthorizationFilter.class)
                .addFilter(new JWTAuthorizationFilter(authenticationManager()));
                //.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
				

    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authenticationservice);
    }
    
    @Override
	public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web)
			throws Exception {
		// TODO Auto-generated method stub
		super.configure(web);
		web
	      .ignoring()
	        .antMatchers(HttpMethod.POST,"/users/save")
	        //.antMatchers(HttpMethod.GET, "/product/list")
	        .antMatchers(HttpMethod.GET, "/product/viewproduct")
	        .antMatchers(HttpMethod.GET, "/product/viewcategory")
	        .antMatchers(HttpMethod.GET, "/product/getcategories");
	}


    @Bean
    public PasswordEncoder passwordEncoder() {
    	return NoOpPasswordEncoder.getInstance();
    }
    
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
        source.registerCorsConfiguration("/**", corsConfiguration);

        return source;
    }
    
}
