package ecommerce.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.user.LoginDataModel;
import ecommerce.user.LoginStatus;
import ecommerce.user.LoginSuccessResponseModel;
import ecommerce.user.LoginToken;
import ecommerce.user.UserDb;
import ecommerce.user.UserRepository;
import ecommerce.utils.Constants;
import lombok.extern.slf4j.Slf4j;



@Slf4j
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

private AuthenticationManager authenticationManager;
private UserRepository repo;
    
    @Autowired
    private Constants constant;
    
    @Autowired
    private ErrorCodes error;
    

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,UserRepository repo) {
        this.authenticationManager = authenticationManager;
        this.repo=repo;
      
      setFilterProcessesUrl("/users/login"); 
    }

    
    @SuppressWarnings("unlikely-arg-type")
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
       
            LoginDataModel creds = null;
			try {
				creds = new ObjectMapper()
				        .readValue(req.getInputStream(), LoginDataModel.class);
			} catch (IOException e) {
				log.error("Invalid request type");
			}
            
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(creds.getEmail_phone(),creds.getPassword(),new ArrayList<>()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException{
    	
    	String email_phone=((User) auth.getPrincipal()).getUsername();
    		UserDb user = null;
    	 
    		UserDb founduser=repo.findByEmail(email_phone);
    		if(founduser!=null) {
    			user=founduser;
    			}
    		else {
    		
	    			UserDb founduser2=repo.findByPhoneno(email_phone);
	    	    	if(founduser2!=null) {
	    	    		user=founduser2;
	    	    	}
	    	    	else {
		    			try {
		    				throw new CustomException(error.USER_DOES_NOT_EXIST);
		    			}
		    			catch(CustomException e) {
		    				log.info(e.getDescription());
		    			}
	    	    	}
    		}
    	
    	
        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername()).withClaim("UserId", user.getId()).withClaim("role",user.getRole())
                .withExpiresAt(new Date(System.currentTimeMillis() + constant.EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(constant.SECRET.getBytes()));
        
        LoginStatus loginstatus=new LoginStatus(true,200);
        LoginToken logintoken=new LoginToken(user.getRole().toString(),user.getFirstname(),token);
        LoginSuccessResponseModel response=new LoginSuccessResponseModel(loginstatus,logintoken);
        
        Gson gson=new Gson();
        String jsonString = gson.toJson(response);

            
        res.getWriter().write(jsonString);
        res.getWriter().flush();
    }
}
