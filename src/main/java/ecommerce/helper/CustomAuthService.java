package ecommerce.helper;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.user.UserDb;
import ecommerce.user.UserRepository;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class CustomAuthService implements UserDetailsService{

	@Autowired
	private UserRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserDb retrievedUser=repo.findByEmail(username);
		
		if(retrievedUser!=null)
		return new User(retrievedUser.getEmail(), retrievedUser.getPassword(), new ArrayList<>());
		else
			{
			
		log.error(username+" not Found in db");
			
		return null;
			}
		
	}

}
