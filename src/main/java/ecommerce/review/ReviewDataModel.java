package ecommerce.review;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class ReviewDataModel {
	
	private Long reviewid;       
	private Long pid;            
	private Long userid;         
	private String comment;		 
	private Integer rating;		 
	private Integer likes;       
}
