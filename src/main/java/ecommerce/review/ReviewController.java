package ecommerce.review;


import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;




@RestController
@RequestMapping("/review")
public class ReviewController {

	@Autowired
	private ReviewService service;
	
	@PostMapping("/save")
	@Roles(roles=Role.BUYER)
	public ReviewDataModel putReview(@RequestBody ReviewDataModel review) throws CustomException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.addReview(userid,review);	
	}
	
	@PostMapping("/delete")
	@Roles(roles=Role.BUYER)
	public List<ReviewDataModel> deleteReview(@RequestBody ReviewDataModel review) throws CustomException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.deleteReview(userid, review);
	}
	
	@PostMapping("/update")
	@Roles(roles=Role.BUYER)
	public ReviewDataModel updateReview(@RequestBody ReviewDataModel review) throws CustomException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.updateReview(userid,review);
	}
	
	
	@GetMapping("/likereview")   
	@Roles(roles=Role.BUYER)
	public void likeReview(@RequestBody ReviewDataModel review) throws CustomException, Exception{
		Long userid=GetUserIdFilter.userid.get();
		service.likeReview(userid,review);
		return;
	}
	
	@GetMapping("/dislikereview")   
	@Roles(roles=Role.BUYER)
	public void dislikeReview(@RequestBody ReviewDataModel review) throws CustomException, Exception{
		Long userid=GetUserIdFilter.userid.get();
		service.dislikeReview(userid,review);
		return;
	}
}
