package ecommerce.review;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Controller;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="Review")
@Controller
@org.hibernate.annotations.TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class ReviewDb implements Comparable< ReviewDb >{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long reviewid;
	
	@Column
	private Long pid;
	
	@Column
	private Long userid;
	
	@Column
	private String comment;
	
	@Column
	private Integer rating;
	
	@Column
	private Integer likes;
	
	 @Override
	 public int compareTo(ReviewDb o) {
		 if (getLikes() == null || o.getLikes() == null) {
		      return 0;
		    }
	        return this.getLikes().compareTo(o.getLikes());
	 }
}
