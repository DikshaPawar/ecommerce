package ecommerce.review;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;



import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.product.ProductDb;
import ecommerce.product.ProductRepository;

import ecommerce.utils.EcommerceUtils;

@Service
public class ReviewService {

	@Autowired
	private ReviewRepository repo;
	
	@Autowired
	private ProductRepository productrepo;
	
	@Caching(evict= {@CacheEvict(cacheNames = "reviewlist", allEntries = true ),@CacheEvict(cacheNames = "avgrating", allEntries = true )})
	public ReviewDataModel addReview(Long userid, ReviewDataModel review) throws CustomException {
		
		Long pid=review.getPid();
		String comment=review.getComment();
		Integer rating=review.getRating();
		if (pid==null || comment==null || rating==null || rating<1 || rating>5) {
			throw new CustomException(ErrorCodes.BAD_REQUEST);
		}
		ProductDb product=productrepo.getOne(pid);
		
		if(product==null)
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		
		review.setUserid(userid);
		review.setLikes(0);
		
		
		ReviewDb savereview=EcommerceUtils.genericConversion(review,new ReviewDb());	

		savereview=repo.save(savereview);
		
		return EcommerceUtils.genericConversion(savereview,new ReviewDataModel());
	
	}
	
	public void likeReview(Long userid,ReviewDataModel review) throws CustomException {
		
		Long reviewid=review.getReviewid();
		if(reviewid==null)
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		ReviewDb rw=repo.findOne(reviewid);
		if (rw==null) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		rw.setLikes(rw.getLikes()+1);
		rw=repo.save(rw);
		return;
	}

	@Caching(evict= {@CacheEvict(cacheNames = "reviewlist", allEntries = true ),@CacheEvict(cacheNames = "avgrating", allEntries = true )})
	public List<ReviewDataModel> deleteReview(Long userid, ReviewDataModel review) throws CustomException {
		
		List<ReviewDb> reviewlist=repo.findByUserid(userid);
		
		Long rid=review.getReviewid();
		if(rid==null) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		if (reviewlist.size()==0) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		
		boolean flag = true;
		
		for(ReviewDb rw:reviewlist) {
			if(rw.getReviewid()==rid) {
				repo.delete(rw);
				flag=false;
			}
		}
		if(flag) {
			throw new CustomException(ErrorCodes.UPDATE_DENIED);
		}
		reviewlist=repo.findByUserid(userid);
		List<ReviewDataModel> updatedlist = new ArrayList<>();
		if(reviewlist==null)
			return updatedlist;
		for(ReviewDb rw:reviewlist) {
			updatedlist.add(EcommerceUtils.genericConversion(rw,new ReviewDataModel()));
		}
		
		return updatedlist;
	}
	
	public void dislikeReview(Long userid, ReviewDataModel review) throws CustomException {
		
		Long reviewid=review.getReviewid();
		if(reviewid==null)
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		ReviewDb rw=repo.findOne(reviewid);
		if (rw==null) {
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		}
		if(rw.getLikes()>0) {
			rw.setLikes(rw.getLikes()-1);
			rw=repo.save(rw);
		}
		return;
	}
	
	@Caching(evict= {@CacheEvict(cacheNames = "reviewlist", allEntries = true ),@CacheEvict(cacheNames = "avgrating", allEntries = true )}) 
	public ReviewDataModel updateReview(Long userid, ReviewDataModel review) throws CustomException {
		
		Long rid=review.getReviewid();
		String comment=review.getComment();
		Integer rating=review.getRating();
		
		review.setUserid(userid);
		
		ReviewDb reviewdb=repo.findOne(rid);
		if(rid==null)
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		if(comment==null || rating==null || rating<1 || rating>5 )
			throw new CustomException(ErrorCodes.BAD_REQUEST);
		if(reviewdb==null)
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);

		
		if(reviewdb.getReviewid().equals(rid) && reviewdb.getUserid().equals(userid)) {
			reviewdb.setRating(rating);
			reviewdb.setComment(comment);

		}
		else {
			throw new CustomException(ErrorCodes.ACCESS_DENIED);
		}
		reviewdb.setLikes(0);
		ReviewDb savereview=repo.save(reviewdb);
		
		return EcommerceUtils.genericConversion(savereview,new ReviewDataModel());
	}

	@Cacheable(value="reviewlist")
	public List<ReviewDataModel> viewReviewOnProduct(Long pid) throws CustomException{
		if (pid==null) {
			throw new CustomException(ErrorCodes.BAD_REQUEST);
		}
		
		List<ReviewDb> list = repo.findByPid(pid);
		List<ReviewDataModel> updatedlist = new ArrayList<>();
		if(list==null) {
			return updatedlist;
		}
		Collections.reverse(list);
		for(ReviewDb rw:list) {
			updatedlist.add(EcommerceUtils.genericConversion(rw,new ReviewDataModel()));
		}
		
		return updatedlist;
	}
	
	
	@Cacheable(value="avgrating")
	public List<Integer> averageRatingOfProduct(Long pid) {
		List<ReviewDb> list= repo.findByPid(pid);
		List<Integer> newlist=new ArrayList<>();
		Integer totalreview=list.size();
		Integer totalscore=0;
		if (totalreview==0) {
			newlist.add(0);
			newlist.add(0);
			return newlist;
		}
		for(ReviewDb rw:list) {
			totalscore+=rw.getRating();
		}
		
		newlist.add(totalreview);
		newlist.add(totalscore);
		return newlist;
	}
	

	
}
