package ecommerce.wishlist;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;



@RestController
@RequestMapping("/wishlist")
public class WishlistController {
	
	@Autowired
	private WishlistService service;
	

	@GetMapping("/list")
	@Roles(roles=Role.BUYER)
	public WishlistDataModel viewWishlist() throws UnsupportedEncodingException, Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.viewWishlist(userid);
	}
	
	@PostMapping("/update")
	@Roles(roles=Role.BUYER)
	public WishlistDataModel updateWishlist(@RequestBody WishlistRequestDataModel wishlist) throws UnsupportedEncodingException, Exception, CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return service.updateWishlist(userid,wishlist);
	}
	
}