package ecommerce.wishlist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface WishlistRepository extends JpaRepository<WishlistDb,Long>{

	WishlistDb findByUserid(Long userid);
}
