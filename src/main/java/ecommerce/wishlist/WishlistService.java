package ecommerce.wishlist;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.product.ProductDataModel;
import ecommerce.product.ProductRepository;
import ecommerce.product.ProductService;
import ecommerce.utils.EcommerceUtils;

@Service
public class WishlistService {

	@Autowired
	private WishlistRepository repo;
	
	@Autowired
	private ProductRepository prepo;
	
	@Autowired
	private ProductService productservice;
	
	
	@CacheEvict(cacheNames = "wishlist", key="#userid" )
	public WishlistDataModel updateWishlist(Long userid,WishlistRequestDataModel wishlist) throws CustomException, UnsupportedEncodingException, Exception {
		WishlistDb wishlistdb = repo.findByUserid(userid);
		
		if(wishlistdb==null)
			wishlistdb=createWishlist(userid);
		
		Long pid = wishlist.getProductid();
		Boolean delete=wishlist.getDelete();
		
		if(pid==null || delete==null) {
			throw new CustomException(ErrorCodes.INVALID_DATA_FORMAT);
		}
		Set<Long> pids=wishlistdb.getPid();
		if(pids==null)
			pids=new HashSet<>();

		for(Long p:pids) {
			if(p==pid) {
				if(delete==true) {
					pids.remove(p);
					wishlistdb.setPid(pids);
					return DbtoDataModel(repo.save(wishlistdb));
				}
				else 
					return DbtoDataModel(wishlistdb); // what should it return this or null
			}
		}
		if(delete==true) {
			return DbtoDataModel(wishlistdb);      // what should it return this or null
		}
		pids.add(pid);
		wishlistdb.setPid(pids);
		wishlistdb=repo.save(wishlistdb);
		return DbtoDataModel(wishlistdb);
	}
	
	@Cacheable(value="wishlist")
	public WishlistDataModel viewWishlist(Long userid) throws UnsupportedEncodingException, Exception {

		WishlistDb wishlist = repo.findByUserid(userid);
		
		if(wishlist==null) {
			wishlist=createWishlist(userid);
		}
		
		return DbtoDataModel(wishlist);
	}
	
	
	public WishlistDb createWishlist(Long userid) {
		WishlistDb createdWishlist=new WishlistDb();
		createdWishlist.setUserid(userid);
		Set<Long> pid=new HashSet<>();
		createdWishlist.setPid(pid);
		return repo.save(createdWishlist);
	}
	
	
	public WishlistDataModel DbtoDataModel(WishlistDb wishlist) throws CustomException {
		
		WishlistDataModel returnWishlist=EcommerceUtils.genericConversion(wishlist,new WishlistDataModel());
		
		Set<Long> oldwishlist=wishlist.getPid();
		Set<ProductDataModel> updatedlist= new HashSet<>();
		
		if(oldwishlist.size()==0) {
			return returnWishlist;
		}
		for(Long pid:oldwishlist) {
			updatedlist.add(convertIdToProduct(pid));
		}
		
		returnWishlist.setProducts(updatedlist);
		return returnWishlist;
	}

	
	public ProductDataModel convertIdToProduct(Long productid) throws CustomException{
		ProductDataModel product=productservice.convertToDataModel(prepo.findOne(productid));
			
		return product;
	}
	
}