package ecommerce.user;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.utils.Constants;
import ecommerce.utils.GetUserIdFilter;





@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService service;
	
	@Autowired
	Constants constant;
	
	@PostMapping("/save")
	public UserDataModel putuser(@RequestBody RequestBodyUser user) throws CustomException {
		
		return service.saveToDb(user);
		
	}
	
	@PostMapping("/update")
	public UserDataModel update(@RequestBody RequestBodyUser user) throws Exception {
		Long userid=GetUserIdFilter.userid.get();
		return service.update(userid,user);

		
	}
	
	
	@GetMapping("/getall")
	public List<UserDataModel> getAll(){
		return service.get();
	}
}
