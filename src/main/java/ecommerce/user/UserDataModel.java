package ecommerce.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class UserDataModel {
	
	//signup class
	public enum Role {
		@SerializedName("B")
	    BUYER,
	    @SerializedName("S")
		SELLER,
		@SerializedName("A")
		ADMIN
	}
	
	private Long id;
	
	private String firstname;
	private String lastname;
	private String phoneno;
	private String email;
	
	
	
	@Autowired
	private Address userAddress; 
	
	private Role role;
}
