package ecommerce.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.gson.Gson;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.utils.EcommerceUtils;
import ecommerce.wallet.UserWalletDb;
import ecommerce.wallet.UserWalletRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private UserWalletRepository walletRepository;
	
	
	
	public UserDataModel saveToDb(RequestBodyUser user) throws CustomException {
			
			UserDb db= EcommerceUtils.genericConversion(user,new UserDb());			//Always use local
			UserDb returnedObject=null;
			try {
				returnedObject=repo.save(db);
			}
			catch(Exception e) {
				log.error(e.getMessage().toString());
				throw new CustomException(ErrorCodes.DB_CONSTRAINT_VIOLATION);
			}
			Long userId=returnedObject.getId();		
			
			String role=returnedObject.getRole();
			
			if(role.substring(0,1).equals("B")) {
			UserWalletDb userWallet=UserWalletDb.builder()   	//wallet created with 0 amount
									.amount(0f)
									.userid(userId)
									.build();
			walletRepository.save(userWallet);
			}
			else {
				log.info("For "+role + " wallet is not created.");
			}
			
			return EcommerceUtils.genericConversion(returnedObject,new UserDataModel());
		}

	public UserDataModel update(Long retrievedUserId,RequestBodyUser user) throws CustomException, JsonProcessingException, IOException{
		
		if(user.getEmail()!=null || user.getRole()!=null) {
			log.info("Cannot update email or role");
			throw new CustomException(ErrorCodes.BAD_REQUEST);
		}
		String userJson=new Gson().toJson(user);
		
		
		UserDb retrievedUser=repo.findOne(retrievedUserId);
		if(retrievedUser==null)
		{	log.error("User not found in database");
			throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);}
		
				ObjectMapper objectMapper = new ObjectMapper();
			    ObjectReader objectReader = objectMapper.readerForUpdating(retrievedUser);  
			      
			    UserDb updatedUser = objectReader.readValue(userJson);
			    
			    return EcommerceUtils.genericConversion(repo.save(updatedUser),new UserDataModel());

			

	}
	
	public List<UserDataModel> get()
	{
		List<UserDataModel> newList=new ArrayList<UserDataModel>();
		
		List<UserDb> list=repo.findAll();
		for(UserDb user:list) {
			newList.add(EcommerceUtils.genericConversion(user,new UserDataModel()));
		}
		return newList;
	}	
	
	
}
