package ecommerce.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class LoginSuccessResponseModel {

	private LoginStatus status;
	private LoginToken userinfo;
	
}
