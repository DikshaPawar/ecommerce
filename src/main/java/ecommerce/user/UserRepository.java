package ecommerce.user;


import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<UserDb,Long>{

	UserDb findByEmail(String username);
	UserDb findByPhoneno(String username);

}
