package ecommerce.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ecommerce.user.UserDataModel.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class RequestBodyUser {
	
	
	private String firstname;
	private String lastname;
	private String phoneno;
	private String email;
	private String password;
	
	
	@Autowired
	private Address userAddress; 
	
	private Role role;
}
