package ecommerce.user;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.springframework.stereotype.Controller;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="users",uniqueConstraints=@UniqueConstraint(columnNames="email"))
@Controller
@org.hibernate.annotations.TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)

public class UserDb {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column		//enum
	private String role;
	
	@Column
	private String lastname;
	
	@Column
	private String password;
	
	@Column
	private String firstname;
	
	
	@Column
	private String phoneno;
	
	@Column
	private String email;
	
	
	@Type(type = "jsonb")
    @Column(name="address",columnDefinition = "jsonb")
	private Address userAddress;

	

}
