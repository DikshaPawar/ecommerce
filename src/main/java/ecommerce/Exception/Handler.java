package ecommerce.Exception;



import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.user.LoginError;
import ecommerce.user.LoginFailedResponseModel;
import ecommerce.user.LoginStatus;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RestController
@ControllerAdvice
public class Handler {
		
		
		
		@ResponseStatus(HttpStatus.BAD_REQUEST)
		@ExceptionHandler({CustomException.class})
		public ExceptionDatamodel defaultException(CustomException e) {
			
			return new ExceptionDatamodel(e.getCode(),e.getDescription());
		}
		
		@ResponseStatus(HttpStatus.BAD_REQUEST)
		@ExceptionHandler(MethodArgumentNotValidException.class)
		public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

		    Map<String, String> errors = new HashMap<>();
		    ex.getBindingResult()
		    	.getFieldErrors()
		    	.forEach(error -> errors.put(error.getField(), error.getDefaultMessage())); 

		    return errors;

		}

		@ExceptionHandler({DataIntegrityViolationException.class,javax.validation.ConstraintViolationException.class,RuntimeException.class})
		public ExceptionDatamodel wrongCredentials(DataIntegrityViolationException e) {
			CustomException exception=new CustomException(ErrorCodes.PRECONDITION_NOT_SATISFIED);
			return new ExceptionDatamodel(exception.getCode(),exception.getDescription());
		}
		
//		@ExceptionHandler({NullPointerException.class})
//		public ExceptionDatamodel wrongCredentials(NullPointerException e) {
//			CustomException exception=new CustomException(ErrorCodes.NULL_VALUE);
//			return new ExceptionDatamodel(exception.getCode(),exception.getDescription());
//		}
		
		@ExceptionHandler({UsernameNotFoundException.class})
		public static LoginFailedResponseModel loginFailed() {
			
			LoginStatus loginstatus=new LoginStatus(false,403);
			 LoginError loginerror=new LoginError("401","Email/Password is invalid");
			 LoginFailedResponseModel response=new LoginFailedResponseModel(loginstatus,loginerror);
			 
			 return response;
		}

}


