package ecommerce.Exception;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Component
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionDatamodel {
	
	private int code;
	private String description;
	
}
