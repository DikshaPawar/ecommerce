package ecommerce.Exception;

import org.springframework.stereotype.Component;

import lombok.Getter;


@Getter
@Component
public class CustomException extends Exception{

		
		@Getter
		public enum ErrorCodes {
			  BAD_REQUEST(400, "BAD REQUEST"),
			  DB_CONSTRAINT_VIOLATION(300,"Db constraint not satisfied"),
			  ACCESS_DENIED(401, "User not allowed for this request"),
			  RESOURCE_NOT_FOUND(404,"RESOURCE NOT FOUND"),
			  INVALID_DATA_FORMAT(409,"UNABLE TO PARSE VALUE"),
			  UNAUTHORIZED_ACCESS(412,"PLEASE SIGN IN AGAIN"),
			  UPDATE_DENIED(417,"YOU CANNOT UPDATE "),
			  PRECONDITION_NOT_SATISFIED(412,"CHECK REQUEST OBJECT "),
			  USER_DOES_NOT_EXIST(400,"USER DOES NOT EXIST IN SYSTEM, PLEASE CHECK EMAIL/PHONENO"),
			  AUTHENTICATION_FAILED(411,"EITHER ID/MOBILE OR PASSWORD IS INVALID"),
			  PRODUCT_LIST_EMPTY(601,"Product list is empty"),
			  WISHLIST_LIST_EMPTY(602,"Wishlist is empty"),
			  CART_LIST_EMPTY(603,"Cart is empty"),
			  PRODUCT_UNAVAILABLE(601,"Product units not left"),
			  NULL_VALUE(612,"Null value found"),
			  NOT_APPLICABLE(613,"Discount conditios not fulfilled"),
			  INSUFFICIENT_BALANCE(600,"Insufficient balance amount");

			  private int code;
			  private String description;
			  
			 ErrorCodes(){}
			  
			  ErrorCodes(int code, String description) {
			    this.code = code;
			    this.description = description;
			  }

			 
			}
		
		private int code;
		private  String description;
		
		public CustomException() {}
		public CustomException(ErrorCodes code ){  
			    this.code=code.getCode();
			    this.description=code.getDescription();
			 }
		
		
}

