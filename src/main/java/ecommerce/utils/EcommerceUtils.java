package ecommerce.utils;


import com.google.gson.Gson;





public class EcommerceUtils {
	

	
	@SuppressWarnings("unchecked")
	public static <S,D> D genericConversion(S source,D destination) {

		String jsonString=new Gson().toJson(source);
		destination=(D) new Gson().fromJson(jsonString, destination.getClass());
		return destination;
		}
	
	
	
}
