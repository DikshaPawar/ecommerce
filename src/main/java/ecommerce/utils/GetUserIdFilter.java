package ecommerce.utils;


	import java.io.IOException;

	import javax.servlet.FilterChain;
	import javax.servlet.ServletException;
	import javax.servlet.ServletRequest;
	import javax.servlet.ServletResponse;
	import javax.servlet.http.HttpServletRequest;

	import org.apache.tomcat.util.codec.binary.Base64;
	import org.json.simple.JSONObject;
	import org.json.simple.parser.JSONParser;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.filter.GenericFilterBean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
	public class GetUserIdFilter extends GenericFilterBean{

		@Autowired
		private static Constants constant;
		
		public static ThreadLocal<Long> userid=new ThreadLocal<>();
		public static ThreadLocal<String> role=new ThreadLocal<>();
		@Override
		public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
				throws IOException, ServletException {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			
			String header = httpRequest.getHeader(constant.HEADER_STRING);
			
			
			String payload=header.split("\\.")[1];
			String decoded=new String(Base64.decodeBase64(payload),"UTF-8");
			
			JSONParser parser = new JSONParser();  
			JSONObject json = null;
			try {
				json = (JSONObject) parser.parse(decoded);
			} catch (Exception e) {
				log.info(e.getMessage());
			} 
			
			
			GetUserIdFilter.userid.set((Long) json.get("UserId"));
			GetUserIdFilter.role.set((String) json.get("role"));
			chain.doFilter(httpRequest, response);
			
		}
		

}
