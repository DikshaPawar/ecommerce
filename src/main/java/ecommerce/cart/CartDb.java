package ecommerce.cart;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="Cart",uniqueConstraints=@UniqueConstraint(columnNames="userid"))
@org.hibernate.annotations.TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)

public class CartDb {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cartid;
	
	@CreationTimestamp
	private Timestamp  createdOn;
	
	@UpdateTimestamp
	private Timestamp processedOn;
	

	@Column		
	private Float amount;

	@Column
	private Long userid;
	
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<CustomPair> product_list;  
	
}
