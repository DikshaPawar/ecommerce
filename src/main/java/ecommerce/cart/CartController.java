package ecommerce.cart;

import java.io.UnsupportedEncodingException;


import javax.validation.Valid;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;



@RestController
@RequestMapping("/users/cart")
public class CartController {
	@Autowired
	CartService service;
	
	
	@GetMapping("/get-cart")
	@Roles(roles=Role.BUYER)
	public CartDataModel getCartDetails() throws UnsupportedEncodingException, CustomException, ParseException {
		Long userid=GetUserIdFilter.userid.get();
		return service.getCartForUser(userid);
	}
	
	
	@PostMapping("/update-cart")
	@Roles(roles=Role.BUYER)
	public CartDataModel updateCart(@RequestBody @Valid CartRequestDataModel usercart) throws UnsupportedEncodingException, ParseException, CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return service.updateCart(userid,usercart);
	}
	
}
