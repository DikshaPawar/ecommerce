package ecommerce.cart;

import java.util.List;

import org.springframework.stereotype.Component;

import ecommerce.product.ProductDataModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class ResponseProduct {

	ProductDataModel product;
	Integer quantity;
}
