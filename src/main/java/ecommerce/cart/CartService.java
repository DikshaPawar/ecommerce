package ecommerce.cart;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;



import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.product.ProductDataModel;
import ecommerce.product.ProductDb;
import ecommerce.product.ProductRepository;
import ecommerce.product.ProductService;

import ecommerce.utils.EcommerceUtils;


@Service
public class CartService {

	@Autowired
	private CartRepository cartrepository;
	@Autowired
	private ProductRepository productrepository;
	
	@Autowired
	private ProductService productservice;
	
	@Cacheable(value="cart")
	public CartDataModel getCartForUser(Long userid) throws CustomException,UnsupportedEncodingException{
		CartDb user_cart=cartrepository.findByUserid(userid);
		
		if(user_cart==null) {
			user_cart=createCart(userid);	
		}
		
		return CartDbToDataModel(user_cart);
	
	}

	@CacheEvict(cacheNames = "cart", key="#userid" )
	public CartDataModel updateCart(Long userid,CartRequestDataModel reqcart) throws UnsupportedEncodingException, CustomException{
	
		CustomPair pair=reqcart.getProduct();
		
		Long productid=pair.getPid();

		if(productrepository.findOne(productid)==null)
			throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
		
		Integer quantity=pair.getQuantity();
		
		CartDb cart=cartrepository.findByUserid(userid);
		
		if(cart==null)
		{
			cart=createCart(userid);
		}

		List<CustomPair> db_list=cart.getProduct_list();
		
		
		for(CustomPair product:db_list)
		{
			Long _productid=product.getPid();
			
			if(_productid==productid)
			{
				product.setQuantity(quantity);
				return CartDbToDataModel(cartrepository.save(cart));
			}
		}
		
		
		db_list.add(pair);
		cart.setProduct_list(db_list);
		
		return CartDbToDataModel(cartrepository.save(cart));
		
	}

	public CartDb createCart(Long userId) {
		
		CartDb createdCart=new CartDb();
		createdCart.setUserid(userId);
		List<CustomPair> empty_list=new ArrayList<>();
		createdCart.setProduct_list(empty_list);
		createdCart.setAmount(0f);
	
		return cartrepository.save(createdCart);
	}

	public ProductDataModel convertIdToProduct(Long productid) throws CustomException{
		ProductDataModel product=productservice.convertToDataModel(productrepository.findOne(productid));
			
		return product;
	}
	
	public Float calculateCartAmount(List<CustomPair> list) throws CustomException {
		Float totalamount=0f;
		
		for(CustomPair pair:list)
		{
			Float amount=productrepository.findOne(pair.getPid()).getListedprice()*pair.getQuantity();
			totalamount+=amount;
		}
		
		return totalamount;
	}
	
	public CartDataModel CartDbToDataModel(CartDb cart) throws CustomException {
		
		List<ResponseProduct> list=new ArrayList<>();
		
		
		CartDataModel resultCart=EcommerceUtils.genericConversion(cart, new CartDataModel());
		
		if(cart.getProduct_list().size()==0)
		{	
			resultCart.setAllproducts(new ArrayList<ResponseProduct>());
			resultCart.setAmount(0f);
			return resultCart;
		}
		
		for(CustomPair pair:cart.getProduct_list())
		{
			if(pair.getQuantity()<1)
			continue;
			ProductDb product=productrepository.findOne(pair.getPid());
			
			if(product==null)
				throw new CustomException(ErrorCodes.PRODUCT_UNAVAILABLE);
			
			ProductDataModel _product=(productservice.convertToDataModel(product));
			list.add(new ResponseProduct(_product,pair.getQuantity()));
		}
		
		resultCart.setAllproducts(list);
		resultCart.setAmount(calculateCartAmount(cart.getProduct_list()));
		
		
		return resultCart;
	}
	
	
}
