package ecommerce.order;

import java.util.List;

import org.springframework.stereotype.Component;

import ecommerce.cart.CustomPair;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class OrderReturnRequestModel {
	
	private Long orderid;
	private CustomPair pair;

}
