package ecommerce.order;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.cart.CustomPair;

import ecommerce.order.OrderDataModel.OrderStatus;
import ecommerce.product.ProductDb;
import ecommerce.product.ProductRepository;
import ecommerce.product.ProductService;
import ecommerce.utils.EcommerceUtils;
import ecommerce.wallet.UserWalletDb;
import ecommerce.wallet.UserWalletRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ProductRepository productrepository;
	
	@Autowired
	private UserWalletRepository walletrepository;
	
	@Autowired
	private ProductService productservice;
	
	
	
		@CacheEvict(cacheNames = "order", key ="#userid")
		public OrderDataModel createUserOrder(Long userid, OrderRequestModel userCart) throws CustomException {
		
			OrdersDb createdOrder=OrdersDb.builder().userid(userid).userAddress(userCart.getAddress()).build();
			
			List<CustomPair> inputList=userCart.getList();
			List<OrderPair> dbList=new ArrayList<>();
			
			Float deliveryCharge=userCart.getTotal_amount()>500?0f:50;
			Float tax=(userCart.getTotal_amount()*1)/100;
			OrderAdditionalInfo addinfo=new OrderAdditionalInfo(tax,deliveryCharge);
			
			Float cart_amount=userCart.getTotal_amount();  //UI will send deducted cart-Amount
			
			if(userid==null) {
				log.error("Token is damaged");
				throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
			}
			if(!SufficientBalance(userid,cart_amount)) {
				dbList=setStatus(inputList,"Declined");
				OrdersDb order_saved=createdOrder.toBuilder().status(OrderStatus.PAYMENT_FAILED.toString())
						.orderamount(cart_amount)
						.productIds(dbList)
						.addinfo(addinfo).build();

				orderRepository.save(order_saved);
				log.error("Insufficient wallet balance");
				throw new CustomException(ErrorCodes.INSUFFICIENT_BALANCE);
				
			}
			if(!quantityLeft(userCart.getList())) {
				dbList=setStatus(inputList,"Declined");
				OrdersDb order_saved=createdOrder.toBuilder().status(OrderStatus.PRODUCT_UNAVAILABLE.toString())
						.orderamount(cart_amount)
						.productIds(dbList)
						.addinfo(addinfo).build();

				orderRepository.save(order_saved);
				log.error("Insufficient product quantity");
				throw new CustomException(ErrorCodes.PRODUCT_UNAVAILABLE);
			}
			
			deductAmount(userid,cart_amount);
			updateQuantity(userCart.getList());
			dbList=setStatus(inputList,"In Transit");
			OrdersDb order_saved=createdOrder.toBuilder().status(OrderStatus.PLACED.toString())
					.orderamount(cart_amount)
					.productIds(dbList)
					.addinfo(addinfo).build();
		
			return DbToDataModel(orderRepository.save(order_saved));
			
		}
		public OrderDataModel DbToDataModel(OrdersDb order) throws CustomException {
			
			
			OrderDataModel returnOrder=EcommerceUtils.genericConversion(order,new OrderDataModel());

			Timestamp ts=order.getCreatedOn(); 
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
            
            returnOrder.setCreatedOn(formatter.format(ts));
            
			List<OrderPair> productids=order.getProductIds();
			
			List<OrderResponseProduct> newList=new ArrayList<>();
			
			if(productids!=null) {
			for(OrderPair pair:productids) {
				Long id=pair.getPid();
				OrderResponseProduct res=new OrderResponseProduct(productservice.convertToDataModel(productrepository.findOne(id)),pair.getQuantity(),pair.getStatus());
				newList.add(res);
			}
			
			returnOrder.setProductsinfo(newList);
			}
			
			return returnOrder;
		}
		
		public boolean SufficientBalance(Long userid, Float amount) throws CustomException {
			UserWalletDb wallet=walletrepository.findByUserid(userid);
			if(wallet==null)
			{	log.error("User wallet with userid : "+userid + " not found in database");
				throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
			}
			Float walletBalance=wallet.getAmount();
			if(amount>walletBalance)
				return false;
			
			return true;
			
		}
		
		public void deductAmount(Long userid, Float amount) throws CustomException {
			UserWalletDb wallet=walletrepository.findByUserid(userid);
			if(wallet==null)
			{	log.error("User wallet with userid : "+userid + " not found in database");
				throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
			}
			wallet.setAmount(wallet.getAmount()-amount);
			walletrepository.save(wallet);
			
		}
		
		public void addAmount(Long userid, Float amount) throws CustomException {
			UserWalletDb wallet=walletrepository.findByUserid(userid);
			if(wallet==null)
			{	log.error("User wallet with userid : "+userid + " not found in database");
				throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
			}
			wallet.setAmount(wallet.getAmount()+amount);
			walletrepository.save(wallet);
			
		}
		
		public boolean quantityLeft(List<CustomPair> list) throws CustomException {
			
			for(CustomPair pair:list) {
				Long id=pair.getPid();
				Integer quantity=pair.getQuantity();
				
				ProductDb product=productrepository.findOne(id);
				if(product==null){	
					log.error("Product with pid : "+id + " not found in database");
					throw new CustomException(ErrorCodes.PRODUCT_UNAVAILABLE);
				}
				if(product.getInfo().getUnitsleft()<quantity)
					return false;
			}
			
			return true;
		}
		
		public void updateQuantity(List<CustomPair> list) throws CustomException {
			for(CustomPair pair:list) {
				Long id=pair.getPid();
				Integer quantity=pair.getQuantity();
				
				ProductDb product=productrepository.findOne(id);
				if(product==null){	
					log.error("Product with pid : "+id + " not found in database");
					throw new CustomException(ErrorCodes.PRODUCT_UNAVAILABLE);
				}
				product.getInfo().setUnitsleft(product.getInfo().getUnitsleft()-quantity);
				product.getInfo().setUnitssold(product.getInfo().getUnitssold()+quantity);
				productrepository.save(product);
				
			}
		}
		
		@Cacheable(value="order")
		public List<OrderDataModel> getAllOrders(Long userid) throws CustomException{
			List<OrdersDb> orderList=orderRepository.findByUserid(userid);
			
			List<OrderDataModel> returnList=new ArrayList<>();
			if(orderList==null) {
				log.info("User with id: "+userid+" have not placed any order.");
				return returnList;
			}
			for(OrdersDb order: orderList) {
				returnList.add(DbToDataModel(order));
			}
			return returnList;
			
		}
		
		public List<OrderPair> setStatus(List<CustomPair> list,String status){
			
			List<OrderPair> dbList=new ArrayList<>();
			
			for(CustomPair pair:list) {
				dbList.add(new OrderPair(pair.getPid(),pair.getQuantity(),status));
				
			}
			return dbList;
		}
		
		@CacheEvict(cacheNames = "order", key ="#userid")
		public OrderDataModel returnSpecificOrder(Long userid,OrderReturnRequestModel returnOrder) throws CustomException {
			
			Long orderid=returnOrder.getOrderid();
			OrdersDb order=orderRepository.findOne(orderid);
			if(order==null){	
				log.error("Order with orderid : "+orderid + " not found in database");
				throw new CustomException(ErrorCodes.RESOURCE_NOT_FOUND);
			}
			CustomPair pair=returnOrder.getPair();
			Long id=pair.getPid();
			Integer quantity=pair.getQuantity();
			
			List<OrderPair> list=order.getProductIds();							
			
			for(OrderPair orderpair:list) {
				
				if(orderpair.getPid()==id) {
						if(orderpair.getQuantity()==quantity) {	
						orderpair.setStatus("Returned");
						}
						else {
							log.error("Return order quantity: "+quantity+ " does not matched with placed quantity : "+orderpair.getQuantity());
							throw new CustomException(ErrorCodes.BAD_REQUEST);
						}
					}
				
			}
				
			
			ProductDb product=productrepository.findOne(id);
			
			addAmount(userid,product.getActualprice()*quantity);
			
			product.getInfo().setUnitsleft(product.getInfo().getUnitsleft()+quantity);
			product.getInfo().setUnitssold(product.getInfo().getUnitssold()-quantity);
			productrepository.save(product);
			
			
			order.setProductIds(list);
			
			return DbToDataModel(orderRepository.save(order));
			
		}
		
}
