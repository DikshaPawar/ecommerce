package ecommerce.order;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.stereotype.Controller;


import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import ecommerce.cart.CustomPair;
import ecommerce.user.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="Orders")
@org.hibernate.annotations.TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Controller
@Builder(toBuilder=true)
public class OrdersDb {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long orderid;
	
	@Column
	private Long userid;
	
	@Column
	private String status; 

	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private List<OrderPair> productIds;
	
	@Column
	private Float orderamount;
	
	@Column
	private String phoneno;
	
	@CreationTimestamp
	private Timestamp  createdOn;
	
	@UpdateTimestamp
	private Timestamp  processedOn;
	
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private OrderAdditionalInfo addinfo;
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private Address userAddress;
	
}
