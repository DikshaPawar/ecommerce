package ecommerce.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;




@RestController
@RequestMapping("/orders")
public class OrdersController {
	
	@Autowired
	private OrderService service;
	
	@PostMapping("/create-order")
	@Roles(roles=Role.BUYER)
	public OrderDataModel createOrder(@RequestBody OrderRequestModel user_cart) throws CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return service.createUserOrder(userid,user_cart);
		
	}
	@GetMapping("/list")
	@Roles(roles=Role.BUYER)
	public List<OrderDataModel> getOrders() throws CustomException{
		Long userid=GetUserIdFilter.userid.get();
		return service.getAllOrders(userid);
	}
	
	@PostMapping("/return-order")
	@Roles(roles=Role.BUYER)
	public OrderDataModel returnOrder(@RequestBody OrderReturnRequestModel returnOrder) throws CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return service.returnSpecificOrder(userid,returnOrder);
		
	}
	

}

