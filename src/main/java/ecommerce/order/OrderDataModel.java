package ecommerce.order;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.google.gson.annotations.SerializedName;

import ecommerce.cart.ResponseProduct;
import ecommerce.user.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class OrderDataModel {
	
	public enum OrderStatus {
		@SerializedName("PAYMENT_FAILED")
	    PAYMENT_FAILED,
	    @SerializedName("PLACED")
		PLACED,
		@SerializedName("PRODUCT_UNAVAILABLE")
		PRODUCT_UNAVAILABLE,
		@SerializedName("PROCESSED")
		PROCESSED,
		@SerializedName("DELIVERED")
		DELIVERED,
		@SerializedName("RETURNED")
		RETURNED
	}

	private long orderid;
	private String  createdOn;
	private float orderamount;
	
	private OrderStatus status;
	
	List<OrderResponseProduct> productsinfo;
	private OrderAdditionalInfo addinfo;
	
	@Autowired
	private Address userAddress;
	
	
}
