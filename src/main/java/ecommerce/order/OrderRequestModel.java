package ecommerce.order;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ecommerce.cart.CustomPair;
import ecommerce.user.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class OrderRequestModel {
	private List<CustomPair> list;
	private Float total_amount;
	
	@Autowired
	private Address address;
	
	private String phoneno;
}

