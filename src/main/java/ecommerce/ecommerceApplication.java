package ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ecommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ecommerceApplication.class, args);
		
	}

}
