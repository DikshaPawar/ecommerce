package ecommerce.coupon;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ecommerce.Exception.CustomException;
import ecommerce.helper.Roles;
import ecommerce.user.UserDataModel.Role;
import ecommerce.utils.GetUserIdFilter;




@RestController
@RequestMapping("/couponcode")
public class CouponCodeController {
	
	@Autowired
	private CouponCodeService couponservice;
	
	@PostMapping("/save")
	@Roles(roles=Role.ADMIN)
	public CouponCodeDataModel createCoupon(@RequestBody CouponCodeRequestModel coupon) throws CustomException {
		Long userid=GetUserIdFilter.userid.get();
		return couponservice.makeCoupon(userid,coupon);
		
	}
	@GetMapping("/list")
	public List<CouponCodeDataModel> getCoupons() {
		Long userid=GetUserIdFilter.userid.get();
		return couponservice.getAllCoupons(userid);
		
	}
	
	@PostMapping("/apply")
	@Roles(roles=Role.BUYER)
	public Float applyCoupon(@RequestBody CartCouponRequestModel coupon) throws CustomException {
		
		return couponservice.applyCode(coupon);
		
	}
	
	

}
