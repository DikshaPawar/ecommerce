package ecommerce.coupon;

import org.springframework.stereotype.Component;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Component
public class CouponCodeRequestModel {
			
	private String code;
	private Float maxdiscount;
	private Float minCartValue;
	private String discountApplicable; //10%,120
}

