package ecommerce.coupon;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
 import lombok.Getter;
 import lombok.NoArgsConstructor;
 import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Component
@Builder
 public class CouponCodeDataModel {
	
 	private String code;
 	private String description;
 	
}