package ecommerce.coupon;

import org.springframework.data.jpa.repository.JpaRepository;



public interface CouponCodeRepository  extends JpaRepository<CouponCodeDb,Long> {
	CouponCodeDb findByCode(String code);
}
