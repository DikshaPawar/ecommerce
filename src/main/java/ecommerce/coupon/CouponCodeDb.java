package ecommerce.coupon;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.stereotype.Controller;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="CouponCodes",uniqueConstraints=@UniqueConstraint(columnNames={"code"}))
@Controller
@Builder
public class CouponCodeDb {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long couponid;
	
	@Column		
	private String code;
	
	@Column
	private String discountApplicable;
	
	@Column
	private Float maxdiscount;
	
	@Column
	private Float minCartValue;
	
	@Column
	private String description;
	


}
