package ecommerce.coupon;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import com.google.gson.Gson;

import ecommerce.Exception.CustomException;
import ecommerce.Exception.CustomException.ErrorCodes;
import ecommerce.user.UserDb;
import ecommerce.user.UserRepository;
import ecommerce.utils.EcommerceUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CouponCodeService {

	@Autowired
	private CouponCodeRepository couponrepo;
	
	@Autowired
	private UserRepository repo;
	
	@CacheEvict(cacheNames = "couponlist",key="#userid")
	public CouponCodeDataModel makeCoupon(Long userid,CouponCodeRequestModel coupon) throws CustomException {
		
		
		if(userid==null)
		{	log.error("Token is damaged");
		throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);}
	
		UserDb user= repo.findOne(userid);
		if(user==null){	
			log.error("User not found in database");
		throw new CustomException(ErrorCodes.AUTHENTICATION_FAILED);
		}
		
		String role=user.getRole();
		
		CouponCodeDb couponCode=CouponCodeDb.builder().code(coupon.getCode())
				.maxdiscount(coupon.getMaxdiscount())
				.minCartValue(coupon.getMinCartValue())
				.discountApplicable(coupon.getDiscountApplicable())
				.build();
		
		String desc="User will get a discount of maximum Rs "+couponCode.getMaxdiscount() +" on a minimum cart value of Rs "+couponCode.getMinCartValue() +"";
		
		couponCode.setDescription(desc);
		
		return EcommerceUtils.genericConversion(couponrepo.save(couponCode),new CouponCodeDataModel());
		
		
		
	}
	
	@Cacheable(value="couponlist")
	public List<CouponCodeDataModel> getAllCoupons(Long userid) {
		
		List<CouponCodeDb> couponlist=couponrepo.findAll();
		
		List<CouponCodeDataModel> returnList=new ArrayList<>();
		
		if(couponlist!=null) {
				for(CouponCodeDb coupon:couponlist) {
					returnList.add(EcommerceUtils.genericConversion(coupon,new CouponCodeDataModel()));
				}
		}
		else 
			log.info("CouponList is empty");
		
		return returnList;
		
	}
	public Float applyCode(CartCouponRequestModel coupon) throws CustomException {
		
		CouponCodeDb retrieved_coupon=couponrepo.findByCode(coupon.getCode());
		Float amount=coupon.getCart_amount();
		
		if(retrieved_coupon==null ||amount < retrieved_coupon.getMinCartValue()) {
			log.error("Coupon code "+coupon.getCode()+" is not applicable.");
			throw new CustomException(ErrorCodes.NOT_APPLICABLE);
				}
		
		Float discount=0f;
		String discountApplicable=retrieved_coupon.getDiscountApplicable();  //50rs,10%
		
		discount=discountApplicableCheck(discountApplicable,amount);
		
		return discount>retrieved_coupon.getMaxdiscount()?retrieved_coupon.getMaxdiscount():discount;
		
	}
	
	public Float discountApplicableCheck(String discount,Float amount) throws CustomException {
		
		char code=discount.charAt(discount.length()-1);
		if(Character.compare(code,'%')==0) {
			
			Float discount_value=Float.parseFloat(discount.substring(0,discount.length()-2));

			if(discount_value<0 || discount_value>100) {
				log.error("Incorrect discount value found: "+discount_value);
				throw new CustomException(ErrorCodes.NOT_APPLICABLE);
			}
			return (discount_value*amount)/100;
			
		}
		
		return Float.parseFloat(discount);
	}
	
}
