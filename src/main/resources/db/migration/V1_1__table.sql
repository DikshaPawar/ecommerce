create table users (
    id SERIAL PRIMARY KEY,
    userAddress jsonb,
    email VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    phoneno VARCHAR(255),
    password VARCHAR(255),
    role VARCHAR(255)
);
create table  coupon_codes  (
    couponid SERIAL PRIMARY KEY,
    code VARCHAR(255),
    discountApplicable VARCHAR(255),
    maxdiscount Real,
    minCartValue REAL,
    description VARCHAR(255)
);
create table cart (
    cartid SERIAL PRIMARY KEY,
    product_list jsonb,
    userid bigint,
    amount REAL,
    createdOn TIMESTAMP,
    processedOn TIMESTAMP
);
create table orders (
    orderid SERIAL PRIMARY KEY,
    addinfo jsonb,
    createdOn TIMESTAMP,
    processedOn TIMESTAMP,
    orderamount REAL,
    userAddress jsonb,
    phoneno VARCHAR(255),
    productIds jsonb,
    status VARCHAR(255),
    userid bigint
);
create table product (
    pid SERIAL PRIMARY KEY,
    name VARCHAR(255),
    category VARCHAR(255),
    userid bigint,
    info jsonb,
    actualprice REAL,
    listedprice REAL,
    visibility BOOLEAN,
    isDeleted BOOLEAN
);
create table review (
    reviewid SERIAL PRIMARY KEY,
    pid bigint,
    userid bigint,
    comment VARCHAR(255),
    rating INTEGER,
    likes INTEGER

);
create table wallet (
    id SERIAL PRIMARY KEY,
    amount REAL,
    userid bigint
);
create table wishlist (
    wishlistid SERIAL PRIMARY KEY,
    userid bigint,
    pid REAL[]
);
